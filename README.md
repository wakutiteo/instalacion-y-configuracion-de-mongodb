# Instalación y configuración de MongoDB

## Instalación

**Nota**: Instalación probada en Ubuntu 18.04,  Debian 10 y Debian 11.

```bash
sudo apt install mongodb
```

## Comprobaciones

### Comprobamos que esté en ejecución

```bash
sudo systemctl status mongodb
```

### Obtener el puerto en escucha

```bash
mongo --eval 'db.runCommand({connectionStatus:1})'
```

## Notas

- Para entrar en la consola de MongoDB

```bash 
mongo
```

- La consola de MongoDB funciona como una de JavaScript.


## Creación y configuración de un usuario con contraseña para acceder a MongoDB

### Creación de un nuevo usuario administrador para todas las BBDD
#### Entramos en la BD `admin`

```javascript
use admin;
```

#### Creamos el usuario administrador

```javascript
db.createUser(
   {
      user: "wakutiteo",
      pwd: "mongo123",
      roles: [{role: "root", db: "admin"}]
   }
);
```

#### Salimos

```javascript
exit
```

### Configuración
#### Descomentamos la linea `#auth = true`

```bash
vim /etc/mongodb.conf 
```

#### Reiniciamos el servicio

```bash
systemctl restart mongodb
# https://askubuntu.com/questions/823288/mongodb-loads-but-breaks-returning-status-14
```

#### Iniciamos una instancia de `mongod` sin control de acceso

```bash
mongod --auth --port 27017 --dbpath /var/lib/mongodb
```

### Creamos un usuario estandar con permisos en una BD en concreto
#### Accedemos con el usuario administrador

```bash
mongo -u "wakutiteo" -p "mongo123" --authenticationDatabase "admin"
```

#### Entramos en la BD en la cual queremos dar permisos al usuario

```javascript
use juegoMongo;
```

#### Creamos el usuario

```javascript
db.createUser(
   {
      user: "eMongo",
      pwd: "emongo123",
      roles: ["readWrite", "dbOwner"]
   }
);
```

#### Como la BD `juegoMongo` todavía no esta creada, debemos insertar algún dato para crearla

```javascript
var juego = {
   titulo: "Witcher 3",
   descripcion: "Juego RPG creado por CDProject",
   fecha_lanzamiento: new Date("2013-05-12")
};
db.juegos.insert(juego);
```

#### Para acceder a la base de datos utilizaríamos

```bash
mongo -u "eMongo" -p "emongo123" --authenticationDatabase "juegoMongo"
```
